import { Pipe, PipeTransform } from '@angular/core';
import { IBranch } from './../app/branch';

@Pipe({
  name: 'filter'
})
export class FilterPipe implements PipeTransform {

  transform(items: IBranch[], searchTerm: string): IBranch[] {
    if(!items) return [];
    if(!searchTerm) return items;
    searchTerm = searchTerm.toLowerCase();
      return items.filter( it => {
        let text = JSON.stringify(it.Name);
          return text.toLowerCase().includes(searchTerm);
      })
  }

}
