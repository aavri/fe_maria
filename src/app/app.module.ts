import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { BranchAPIService } from './service/branch-api.service';

import { AppComponent } from './app.component';
import { ListBranchesComponent } from './list-branches/list-branches.component';

import { NgxPaginationModule } from 'ngx-pagination';
import { FilterPipe } from '../pipes/filter.pipe';
import { SearchBarComponent } from './search-bar/search-bar.component';
import { BranchDetailsComponent } from '../shared/branch-details/branch-details.component';

@NgModule({
  declarations: [
    AppComponent,
    ListBranchesComponent,
    FilterPipe,
    SearchBarComponent,
    BranchDetailsComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    NgxPaginationModule
  ],
  providers: [
    BranchAPIService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
