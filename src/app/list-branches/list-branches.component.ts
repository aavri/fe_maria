import { Component, OnInit, ViewChild } from '@angular/core';
import { BranchAPIService } from '../service/branch-api.service';
import { BranchDetailsComponent } from '../../shared/branch-details/branch-details.component';

@Component({
  selector: 'app-list-branches',
  templateUrl: './list-branches.component.html',
  styleUrls: ['./list-branches.component.css'],
  providers: [BranchAPIService]
})
export class ListBranchesComponent implements OnInit {

  records = [];
  branches = [];
  hideme = [];

  isLoading: boolean = true;
  isOpen: boolean = false;

  searchTerm: string;

  @ViewChild('branchDetails') branchDetails: BranchDetailsComponent;

  constructor(private branchAPIService: BranchAPIService) { }

  ngOnInit() {
    this.getList();
  }

  getList() {
    this.branchAPIService.getData()
    .subscribe(data => {
      this.isLoading = false;
      const branchObj = data.data[0].Brand[0].Branch;
      branchObj.forEach(record => {
        this.records = this.assignRecord(record);
      })
    })
  }

  assignRecord(record) {
    record._recordName = record;
    this.branches.push(record._recordName);
    return this.branches;
  }

  getSearchTerm(event) {
    this.searchTerm = event;
    return this.searchTerm;
  }

}

  

