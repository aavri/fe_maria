import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { Subject } from 'rxjs';
import { debounceTime, distinctUntilChanged } from 'rxjs/operators';

@Component({
  selector: 'app-search-bar',
  templateUrl: './search-bar.component.html',
  styleUrls: ['./search-bar.component.css']
})
export class SearchBarComponent implements OnInit {
  @Output() filterTrigger = new EventEmitter<any>();
  searchTerm: any;
  searchTermFilter: Subject<any> = new Subject<any>();

  constructor() { }

  ngOnInit() {
    this.searchTermFilter.pipe(
      debounceTime(100),
      distinctUntilChanged()).subscribe(data => {
        this.filterTrigger.emit(data);
      })
  }

  search(event) {
    this.searchTermFilter.next(event);
  }

}
